---
title: Contact
author_profile: true
permalink: /contact/
---
<span style="color:red">Pas opérationnel !</span>

Parce que ce blog est avant tout un moyen d'échanger et de réfléchir sur la Nature et tout ce qui lui est lié au possible, voici un formulaire pour nous contacter.
 
N'hésitez pas à nous faire par de vos idées: rectifications, identifications erronées, informations, questions ... nous sommes tout ouïe et nous tenterons de vous répondre le plus rapidement possible. ~~Sinon, laissez nous un commentaire sous l'article de votre choix ou plus simplement abonnez-vous à la Newsletter pour avoir toutes les infos en temps et en heure.~~

<form method="POST" action="https://formspree.io/<YOUR_EMAIL_HERE>">
  <input type="email" name="email" placeholder="Your email">
  <input type="text" name="name" placeholder="Your name">
  <textarea name="message" placeholder="Your message" rows="3">
  </textarea>
  <button type="submit">Send Message</button>
</form>

Pour pouvez également nous contacter en message privé par nos emails.