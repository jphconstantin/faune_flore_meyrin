---
layout: archive
title: A Propos
classes: wide
permalink: /about/
author: jpc
---

Deux passionnés ...   

L'un polytechnicien, mordu de photographie, curieux et cartésien de nature, révèle les trésors de la vie qui nous entoure, toujours à la recherche de l'explication scientifique.   

L'autre universitaire, biologiste dans l'âme, sensible à la fragilité de l'écosystème, toujours prêt à sauvegarder notre biodiversité, il connaît les mystères de la nature et vous montre l'invisible.   

Tout deux essayent d'avoir une attitude respectueuse lors de leurs randonnées dédiées à l'observation de la faune et de la flore.   
Tout deux habitant Meyrin depuis 1962, ils ont vécu la métamorphose de la commune, le passage d'un village à une cité.

L'évolution des habitats et de l'urbanisation ont progressé et, par voie de conséquence, les surfaces naturelles ont continué à se réduire comme peau de chagrin.

Ils ont vu malheureusement des espèces disparaître, comme le lézard des souches et les sonneurs à ventre rouge. 

Mais cela n'empêche pas la Nature de reprendre ses droits: on découvre des espèces d'orchidée dans des endroits insolites.

Ces deux-là, épicuriens qui plus est, ne pouvaient que se rencontrer, pour le meilleur, sans aucun doute.


[Mettre deux photos]
