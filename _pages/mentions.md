---
layout: archive
title: Mentions Légales
permalink: /mentions/
classes: wide
---

Tous les textes et photos, sauf indication contraire, sont sujets aux lois de la protection intellectuelle et 
sont la propriété de © Victor Berridge & Jean-Philippe Constantin.

Aucune photographie ne peut être reproduite, téléchargée, copiée, stockée, dérivée ou utilisée en partie ou en intégralité, sans permission écrite des propriétaires.

Tous droits réservés.

Faune et Flore de Meyrin<br>
Responsable: Jean-Philippe Constantin<br>
Adresse postale: 59 avenue de Mategnin 1217 MEYRIN (GE) - SUISSE<br>
Adresse e-mail: jph.constantin1217[at]gmail.com<br>
