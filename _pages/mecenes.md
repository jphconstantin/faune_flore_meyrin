---
layout: archive
title: 
classes: wide
permalink: /mecenes/
---

Nous nous consacrons à notre échelle à faire connaître aux habitants de la commune de Meyrin et à toutes autres personnes la richesse de la biodiversité locale.
C'est un travail, non rémunéré, entrepris par deux amoureux de la Nature.

Pour l'instant, le site est localisé sur un hébergeur gratuit [https://github.com](https://github.com).

Qui dit gratuit dit contraintes. 

En effet, la taille mise à disposition est limitée et le grand nombre de photographies que l'on stocke va vite atteindre la limite fatidique.
Les photographies, même après avoir subies une compression prennent énormément de place.

Nous souhaitons nous affranchir de ces contraintes en hébergeant ce site chez un hébergeur digne de ce nom.

- Voulez-vous vous associer à une démarche écologique saine ?
- Etes-vous un passionné de la faune et la flore ? 
- Aimez-vous la commune de Meyrin au point de contribuer un peu plus à son rayonnement ?

Alors soutenez-nous dans notre entreprise !

Jean-Philippe CONSTANTIN    
Victor BERRIDGE